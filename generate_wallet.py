# -*- coding: utf-8 -*-

from py_crypto_hd_wallet import HdWallet, HdWalletFactory, HdWalletSaver, HdWalletCoins, HdWalletWordsNum, HdWalletChanges, HdWalletDataTypes, HdWalletKeyTypes


def create_new_wallet():
    '''returns a new py_crypto_hd_wallet from a randomly generated mnemonic'''
    hd_wallet_fact = HdWalletFactory(HdWalletCoins.ETHEREUM)
    hd_wallet = hd_wallet_fact.CreateRandom("eth_wallet_from_random", HdWalletWordsNum.WORDS_NUM_24)
    print(hd_wallet.ToJson())
    return hd_wallet

def create_wallet_from_mnemonic(mnemonic):
    '''returns a new py_crypto_hd_wallet from a user supplied mnemonic'''
    hd_wallet_fact = HdWalletFactory(HdWalletCoins.ETHEREUM)
    hd_wallet = hd_wallet_fact.CreateFromMnemonic("eth_wallet_from_mnemonic", mnemonic)
    return hd_wallet 

def create_wallet_from_xpub(_xpub):
    '''returns new py_crypto_hd_wallet object from an xpub'''
    hd_wallet_fact = HdWalletFactory(HdWalletCoins.ETHEREUM)
    hd_wallet = hd_wallet_fact.CreateFromExtendedKey("eth_wallet_from_xpub",_xpub)
    return hd_wallet

def generate_wallet_addresses(hd_wallet, total_addresses):
    '''generate addresses from hd_wallet object'''
   
    hd_wallet.Generate(account_idx = 0, change_idx = HdWalletChanges.CHAIN_EXT, addr_num = total_addresses)
    return hd_wallet

def print_wallet_addresses(_hd_wallet_with_addresses):
    '''Print all wallet addresses'''
    addy_number = 0 
    addresses = _hd_wallet_with_addresses.GetData(HdWalletDataTypes.ADDRESSES)
    for addr in addresses:
        print(addy_number, addr.GetKey(HdWalletKeyTypes.ADDRESS), addr.GetKey(HdWalletKeyTypes.RAW_PRIV))
        addy_number += 1
   
def get_xpub_from_hd_wallet(_hd_wallet):
    '''Given an hd_wallet, returns the xpub for the change address as per BIP 44 spec
    master / purpose / coin_type / account / change / address_index
    ''' 
    hd_wallet_change_key = _hd_wallet.GetData(HdWalletDataTypes.CHANGE_KEY)
    hd_wallet_change_key_dict = hd_wallet_change_key.ToDict()
    xpub = hd_wallet_change_key_dict["ex_pub"]
    print('change key xpub: {0}'.format(xpub))
    return xpub 

# create new wallet from new random mnemonic 
print('New wallet from randomly generated mnemonic & generate some addys ⟹')
wallet = create_new_wallet()
wallet_with_addys = generate_wallet_addresses(wallet, 5)
print_wallet_addresses(wallet)
print('\n')

# create wallet from existing mnemonic 
print('Create new wallet from existing mnemonic & generate some addys ⟹')
mnemonic = 'dirt reform youth crop smooth melt wrong taxi aerobic priority desk cancel employ suffer treat bean worry daring guess void leaf roast alien black'
print('mmemonic: {0}'.format(mnemonic))
wallet = create_wallet_from_mnemonic(mnemonic)
wallet_with_addys = generate_wallet_addresses(wallet, 5)
print_wallet_addresses(wallet_with_addys)
print('\n')

# extract change key xpub from hd_wallet and print addys 
print('Extract xpub (change key) from existing mnemonic & generate some addys ⟹')
mnemonic = 'dirt reform youth crop smooth melt wrong taxi aerobic priority desk cancel employ suffer treat bean worry daring guess void leaf roast alien black'
print('mmemonic: {0}'.format(mnemonic))
wallet = create_wallet_from_mnemonic(mnemonic)
wallet_with_addys = generate_wallet_addresses(wallet, 5)
xpub = get_xpub_from_hd_wallet(wallet_with_addys)
print_wallet_addresses(wallet_with_addys)
print('\n')

# given an existing xpub, generate N addresess. Used for insecure envs where private keys and mnemonics aren't safe
print('Generate some addys from existing xpub (change key) ⟹')
xpub = 'xpub6Edh496D4eQ5jkbmZouZQKTvfX2RDCxGtDnzzLDi7WZ9k2VvQtYTBN5YpAdqp1rwmEPVKc4SpC2PnNCzghbAejjVYYWfyMM4SngCPrgvtQK'
print('xpub: {0}'.format(xpub))
wallet = create_wallet_from_xpub(xpub)
wallet_with_addys = generate_wallet_addresses(wallet, 5)
print_wallet_addresses(wallet_with_addys)
print('\n')






